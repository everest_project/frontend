#!/bin/bash

DEPLOY_SERVER=64.226.76.24
SERVER_FOLDER="mathsat.uz"

# Building React output
yarn install
yarn run build

echo "Deploying to ${DEPLOY_SERVER}"
scp -r build/ user_1@${DEPLOY_SERVER}:/var/www/${SERVER_FOLDER}/

echo "Finished copying the build files"
